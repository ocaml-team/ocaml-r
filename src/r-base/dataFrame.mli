(**  Constructs a {!R.Base.dataframe} object from a R data frame. *)
class dataframe_from_R : R.Base.dataframe R.t -> R.Base.dataframe

