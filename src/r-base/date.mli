(** Constructs an {!R.Base.date} from an R date. *)
class date_from_R : R.Base.date R.t -> R.Base.date

