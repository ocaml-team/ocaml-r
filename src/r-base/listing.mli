(**  Constructs an {!R.Base.listing} from an R list. *)
class listing_from_R : R.Base.listing R.t -> R.Base.listing

